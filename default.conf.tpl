server {
  server_name jaygray.gratis www.jaygray.gratis;

  listen ${LISTEN_PORT};

  location /static {
    alias /vol/static;
  }

  location /app {
    proxy_pass http://${APP_HOST}:${APP_PORT};
  }

}