# API Proxy

NGINX proxy app for API

## Usage

### Environment Variables

- `LISTEN_PORT` - Port to listen on. (default: `8000`)
- `APP_HOST` - Hostname of app to forward requests to (default: `app`)
- `PORT` - Port of the app to forward requests to (default: `9000`)1
  EX:

git tag -a v0.1.43-release -m "v0.1.43" && git push --tags
