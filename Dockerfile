FROM nginxinc/nginx-unprivileged:1-alpine
LABEL maintainer="jeremygray001@gmail.com"

COPY ./default.conf.tpl /etc/nginx/default.conf.tpl

ENV LISTEN_PORT=8000
ENV APP_HOST=apptest
ENV APP_PORT=9000

ENV SECOND_APP_HOST=secondapphost
ENV SECOND_APP_PORT=3030

USER root

RUN mkdir -p /vol/static
RUN chmod 755 /vol/static
RUN touch /etc/nginx/conf.d/default.confg
RUN chown nginx:nginx /etc/nginx/conf.d/default.conf

COPY ./entrypoint.sh /entrypoint.sh
RUN chmod +x /entrypoint.sh

USER nginx

CMD ["/entrypoint.sh"]